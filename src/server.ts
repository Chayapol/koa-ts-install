import { Application } from "./Application";
import Configuration from "./Configuration";

export default new Application().start(Configuration.server.port);