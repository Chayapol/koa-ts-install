export const Configuration = {
    server: {
        port: 3000,
    },
    app: {
        appVersion: 1,
        rootPrefix: "/api/",
        get rootUrl() {
            return this.rootPrefix + "v" + this.appVersion;
        },
    }
}

export default Configuration;